#!/usr/bin/env python
import sys
import pika

# Establish connection to RabbitMQ server
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Creates hello queue
channel.queue_declare(queue='hello')

# Exchange = default
# Routing key = queue
# body = "Message"
channel.basic_publish(exchange='', routing_key='hello', body=sys.argv[1])

print(" [x] Sent " + sys.argv[1])

# Close Connection
connection.close()

